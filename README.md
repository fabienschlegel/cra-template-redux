# cra-template-redux

Custom template for [Create React App](https://github.com/facebook/create-react-app)

## Usage

```sh
npx create-react-app my-app --template @nixtech/redux
```
